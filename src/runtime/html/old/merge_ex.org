#+TITLE: Demonstration of Merge Step Exercise Artefact (HTML)
#+AUTHOR: Mernedi Venkata Naga Narasimha Ashish
#+DATE: 2019-05-23 Wed
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This document builds the =merge step exercise= interactive
  artefact(HTML).

* Features of the artefact
+ Artefact provides demo for split step.
+ User can see a randomly generated two subarray of size 5 each in the
  beginning. Size of the array is fixed.
+ User can click the =Reset= button to generate a new array
  at any time.
+ User can click on =Array1= button to select as new element 
  if subarray1 has the smaller element being pointed.
+ User can click on =Array2= button to select as new element 
  if subarray2 has the smaller element being pointed.

* HTML Framework of Simple Bubble Sort
** Head Section Elements
Title, meta tags and all the links related to Js, CSS of
this artefact comes under this section.
#+NAME: head-section-elems
#+BEGIN_SRC html
  <title>Bubble Sort</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="../css/mergesort.css">
  <script src="../js/merge_ex.js"></script>
#+END_SRC

** Body Section Elements
*** Instruction box
This is an instruction box, in which the instructions needed
for the user to perform the experiments are displayed in a
form of list.
#+NAME: instruction
#+BEGIN_SRC html

  <div class="instruction">
    <b>Instructions</b>: 
    <ul>
    <li>Look at the pointers Pointer1 and Pointer2. Click on the Array1 or Array2 buttons to get the sorted merged array. If the element pointed by Pointer1 is smaller, click on Array1 and if the element pointed by Pointer2 is smaller, click on Array2.</li> 
    <li>Continue this process till you get the completely sorted merged array.</li>
    <li>If your answer is wrong, you will see a message indicating the same.</li>
    </ul> 
  </div>

#+END_SRC

*** Canvas
This is an canvas, which is used for graphics.
#+NAME: canvas
#+BEGIN_SRC html

    <div>
      <canvas id="canvas" width="400" height="240"></canvas>
    </div>

#+END_SRC

*** Controller Elements
This are the controller buttons(Next Step, Reset, Previous Step) of an
artefacts used for performing/demonstrating the artefact.
#+NAME: controller-elems
#+BEGIN_SRC html
<div id="buttondiv" >
  <center>
    <button type="button" class = "button" id="array1">Array 1</button>
    <button type="button" class = "button" id="array2">Array 2</button>
    <button type="button" class = "button" id="reset">Reset</button>
  </center>
</div>

#+END_SRC

* Tangle
#+BEGIN_SRC html :tangle merge_ex.html :eval no :noweb yes

    <!DOCTYPE HTML>
    <html lang="en">
      <head>
        <<head-section-elems>>
      </head>
      <body onload="reset()" onresize="draw()">
        <<instruction>> 
        <<canvas>>
        <<controller-elems>>
      </body>
    </html>

#+END_SRC
